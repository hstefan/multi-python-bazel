#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import grpc
import time

from concurrent import futures
from multi_python_bazel.util import conf
from multi_python_bazel.proto import helloworld_pb2_grpc
from multi_python_bazel.proto.helloworld_pb2 import HelloRequest, HelloReply


_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class _GreeterServer(object):

    def __init__(self, greeter_service, server_port):
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        helloworld_pb2_grpc.add_GreeterServicer_to_server(greeter_service, self.server)
        self.server.add_insecure_port('[::]:{server_port}'.format(server_port=server_port))

    def start(self):
        self.server.start()

    def stop(self):
        self.server.stop(0)

    def await_termination(self):
        """
        server.start() doesn't block so we explicitly block here unless someone keyboard-exits us.
        :return:
        """
        try:
            while True:
                time.sleep(_ONE_DAY_IN_SECONDS)
        except KeyboardInterrupt:
            self.server.stop(0)
        pass


def make_hello_string(name):
    assert name is not None
    return "Hello, {}!".format(name)


class _GreeterService(helloworld_pb2_grpc.GreeterServicer):
    def SayHello(self, hello_request, context):
        print("< %s" % hello_request.name)
        reply = HelloReply(message=make_hello_string(hello_request.name))
        return reply


if __name__ == "__main__":
    greeter_server = _GreeterServer(_GreeterService(), conf.GREETER_PORT)
    greeter_server.start()
    greeter_server.await_termination()