import pytest
import sys
from multi_python_bazel.server.app import make_hello_string


def test_make_hello_string():
    assert make_hello_string("Stefan") == "Hello, Stefan!"


def test_make_hello_string_raise():
    with pytest.raises(AssertionError):
        make_hello_string(None)


def test_make_hello_string_mismatch():
    assert make_hello_string("Stefan") != "Hello, Rando!"


if __name__ == "__main__":
    sys.exit(pytest.main([__file__]))