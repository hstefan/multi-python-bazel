#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import grpc

from multi_python_bazel.proto.helloworld_pb2 import HelloRequest
from multi_python_bazel.proto import helloworld_pb2_grpc
from multi_python_bazel.util import conf


if __name__ == '__main__':
    channel = grpc.insecure_channel('{}:{}'.format(conf.GREETER_HOST, conf.GREETER_PORT))
    stub = helloworld_pb2_grpc.GreeterStub(channel)
    while True:
        name = raw_input("> ")
        response = stub.SayHello(HelloRequest(name=name))
        print("< " + response.message)
